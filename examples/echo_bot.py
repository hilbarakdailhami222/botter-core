import os
import random

from botter.api import *
from botter.api.handlers import *
from botter.telegram import TelegramBot

def first_line(msg: str) -> str:
    lines = msg.strip().splitlines()
    if (lines):
        return lines[0].strip()
    else:
        return ''


class HelloHandler(ReplyHandler):
    async def can_handle(self, event: Event) -> bool:
        """
        Function `can_handle` states if current handler can handle specific event.
        For `HelloHandler`, it will return True if event is `MessageEvent` (inherited)
        and its first line is either '!hello' or '!hi'
        """
        
        if (not await super().can_handle(event)):
            return False
        event: MessageEvent
        return first_line(event.message.text) in { '!hello', '!hi' }
    
    async def handle_message(self, message: InboundMessage) -> Message:
        """
        `ReplyHandler`'s handle_message method could return Message.
        It it is the case, it would reply to the inbound message.
        """
        
        print(f"HelloHandler: Handling message: {message}")
        return Message("Hello!")

class SimpleEchoHandler(ReplyHandler):
    async def handle_message(self, message: InboundMessage) -> Message:
        print(f"SimpleEchoHandler: Handling message: {message}")
        return Message(f"You've said:\n" + message.text)

class SmartEchoHandler(ReplyHandler):
    async def handle_message(self, message: InboundMessage) -> Message:
        """
        Additionally, `Message` class provides attachments field.
        `.attachments` is list of `Attachment`.
        Here we check if the message has attached image, and if so, reply a random flower.
        """
        
        print(f"SmartEchoHandler: Handling message: {message}")
        if (message.has_attachments):
            if (message.has_image):
                flowers_dir = os.path.join(os.path.dirname(__file__), 'flowers')
                flowers = os.listdir(flowers_dir)
                # noinspection PyTypeChecker
                at = FileAttachment(AttachmentType.Image, os.path.join(flowers_dir, random.choice(flowers)))
                return Message(f"Wow! You've send me an image!\n" "I also can send images!", attachments=[at])
            elif (len(message.attachments) == 1):
                at = message.attachments[0]
                return Message(f"Wow! You've send me an attachment '{at.filename}', which weights {at.weight} bytes")
            else:
                attachment_names = ', '.join(f"'{at.filename}'" for at in message.attachments)
                return Message(f"You've send me {len(message.attachments)} documents: {attachment_names}, with a total size of {sum(at.weight for at in message.attachments)} bytes")
        else:
            return Message(f"You've said:\n" + message.text)

class WhoAmIHandler(StartHandler):
    async def handle_event(self, event: StartEvent):
        """
        This event is handled only once - upon bot start
        and states that bot is ready to receive events.
        
        Current implementation will print system information to the stdout.
        """
        
        print('Logged in as')
        print(event.bot_user.display_name)
        print(event.bot_user.id)
        print('------')

class EchoBot(Bot[TelegramBot]):
    token = '123:INSERT_YOUR_TOKEN_HERE'
    event_handlers = [ HelloHandler, SmartEchoHandler, WhoAmIHandler ]
    client = TelegramBot(token=token)

if (__name__ == '__main__'):
    EchoBot.run()
